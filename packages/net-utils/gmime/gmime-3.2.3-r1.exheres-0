# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ] vala [ vala_dep=true with_opt=true ]

SUMMARY="Set of utilities for parsing and creating messages using the
Multipurpose Internet Mail Extension (MIME)"
HOMEPAGE="https://github.com/jstedfast/gmime"

LICENCES="LGPL-2.1"
SLOT="3.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    idn
"
DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.16]
        gtk-doc? ( dev-doc/gtk-doc[>=1.8] )
    build+run:
        app-crypt/gpgme[>=1.8.0]
        dev-libs/glib:2[>=2.32]
        sys-libs/zlib
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.30.0] )
        idn? ( net-dns/libidn2:=[>=2.0.0] )
    test:
        app-crypt/gnupg
"

DEFAULT_SRC_CONFIGURE_PARAMS=( '--enable-crypto' )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'gtk-doc'
    'vapi vala'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'idn libidn'
)

src_test() {
    esandbox allow_net --connect "unix:/${WORK}/tests/tmp/.gnupg/S.gpg-agent*"
    esandbox allow_net "unix:/${WORK}/tests/tmp/.gnupg/S.gpg-agent*"

    default

    esandbox disallow_net "unix:/${WORK}/tests/tmp/.gnupg/S.gpg-agent*"
    esandbox disallow_net --connect "unix:/${WORK}/tests/tmp/.gnupg/S.gpg-agent*"
}

