# Copyright 2009 David Hilton
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.gz ] freedesktop-desktop gtk-icon-cache
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="Tool for graphical partition management"
DESCRIPTION="
The GParted application is the GNOME partition editor for creating, reorganizing, and deleting disk
partitions.

A disk device can be subdivided into one or more partitions. The GParted application enables you to
change the partition organization on a disk device while preserving the contents of the partitions.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    device-mapper [[ description = [ Add support for dmraid ] ]]
    doc
    polkit
    ( linguas: ar be bg br bs ca ca@valencia cs da de dz el en_CA en_GB eo es et eu fa fi fr fur gd
               gl gu he hr hu id is it ja kk ko lt lv mk ml nb ne nl nn oc pa pl pt_BR pt ro ru rw
               si sk sl sr sr@latin sv te th tr uk vi zh_CN zh_HK zh_TW )
"

# gparted will also use the following packages, but exheres do not yet exist: hfsutils, hfsprogs, udftools
DEPENDENCIES="
    build:
        dev-util/intltool[>=0.36.0]
        sys-devel/gettext
        virtual/pkg-config[>=0.9.0]
        doc? ( gnome-desktop/yelp-tools )
    build+run:
        dev-cpp/libsigc++[>=2.5.1]
        dev-libs/glib:2 [[ note = [ for gthread ] ]]
        gnome-bindings/glibmm:2.4[>=2.45.40]
        gnome-bindings/gtkmm:3[>=3.22.0]
        sys-apps/util-linux [[ note = [ for libuuid and MINIX file system support ] ]]
        sys-fs/parted[>=3.2][device-mapper?] [[ note = [ 3.2 for online resizing ] ]]
        polkit? ( sys-auth/polkit[>=0.102] )
    suggestion:
        sys-apps/hdparm [[
            description = [ Required to display serial number device information ]
        ]]
        sys-fs/btrfs-progs[>=4.1] [[ description = [ Support for Btrfs filesystems ] ]]
        sys-fs/cryptsetup         [[ description = [ Support for LUKS encrypted volumes ] ]]
        sys-fs/dosfstools         [[ description = [ Support for FAT16/32 filesystems ] ]]
        sys-fs/f2fs-tools         [[ description = [ Support for F2FS filesystems ] ]]
        sys-fs/jfsutils           [[ description = [ Support for JFS filesystems ] ]]
        sys-fs/lvm2               [[ description = [ Support for LVM volumes ] ]]
        sys-fs/mtools [[
            description = [ Required to read/write FAT16/32 volume labels and UUIDs ]
        ]]
        sys-fs/nilfs-utils        [[ description = [ Support for NILFS filesystems ] ]]
        sys-fs/ntfs-3g_ntfsprogs  [[ description = [ Support for NTFS filesystems ] ]]
        sys-fs/reiserfsprogs      [[ description = [ Support for ReiserFS filesystems ] ]]
        sys-fs/reiser4progs       [[ description = [ Support for Reiser4 FS filesystems ] ]]
        sys-fs/xfsprogs           [[ description = [ Support for XFS filesystems ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-online-resize
    --disable-static
    --disable-xhost-root
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'device-mapper libparted-dmraid'
    doc
)

src_prepare() {
    if option polkit; then
        expatch "${FILES}"/force-enable-polkit.patch
    fi

    autotools_src_prepare
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

